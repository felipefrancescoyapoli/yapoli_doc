# DOCUMENTAÇÃO TEMPORÁRIA - API #
URL Base : https://wnb9rzlqtb.execute-api.sa-east-1.amazonaws.com/prod/api

Service GetData
URL
/getData

Parametros Obrigatórios
Slug client_slug=beta
Parametros Opcionais
Extension extension=png extension=png,jpg,psd

Nome do arquivo original original_name=teste.jpg original_name=tes original_name=jpg

Service DeleteFile
URL
/deleteFile

Parametros Obrigatórios
Id Asset id=1be25206-cc14-48e2-822b-84051a310d1d

## Service GetData
### URL
/getData

#### Parametros Obrigatórios
* Slug
    ```
    client_slug=beta
    ```
    
#### Parametros Opcionais
* Extension
    ```
    extension=png
    extension=png,jpg,psd
    ```
    
* Nome do arquivo original
    ```
    original_name=teste.jpg
    original_name=tes
    original_name=jpg
    ```
	
## Service DeleteFile
### URL
/deleteFile

#### Parametros Obrigatórios
* Id Asset
    ```
    id=1be25206-cc14-48e2-822b-84051a310d1d
    ```